# Javascript Implementation of Extended Markdown Tables

This is an implementation in vanilla Javascript for
[extended markdown tables](https://gitlab.com/liberdade-controle-automacao/extended-markdown-table).

Usage example:

``` js
let xmdt = new ExtendedMarkdownTable();
let markdown = `| name | age | job |
|----|----|----|
| Mira | 27 | Superhero |
| Pietro | 24 | Clown |
| Sprocket | 17 | Robot |
: title = (t, r, i) => \`\${r["name"]}, the \${r["job"]}\`
`;
// this should look like an array of objects
let table = xmdt.extend(markdown);
// while this is the markdown string
let extendedMarkdown = xmdt.toMarkdown(table);
```
