const example1 = `| name | age | job |
|----|----|----|
| Mira | 27 | Superhero |
| Pietro | 24 | Clown |
| Sprocket | 17 | Robot |
: title = (t, r, i) => \`\${r["name"]}, the \${r["job"]}\`
`;

const example2 = `| city | hood | food | commute | rent |
|------|------|------|---------|------|
| lisbon | areeiro | 700 | 0 | 900 |
| porto | boavista | 650 | 0 | 700 |
| amsterdam | almere | 900 | 6 | 1100 |
| amsterdam | haarlem | 1000 | 7 | 1600 |
| amsterdam | zaandam | 900 | 7 | 800 |
| tokyo | kashiwa | 1000 | 3 | 360 |
| tokyo | akabane | 1000 | 3 | 600 |
| japan | osaka | 900 | 0 | 500 |
| japan | sendai | 900 | 0 | 400 |
| japan | kitakyushu | 900 | 0 | 500 |
: transportation = (t, r, i) => 23 * parseInt(r["commute"])
: total = (t, r, i) => parseInt(r["food"]) + parseInt(r["transportation"]) + parseInt(r["rent"])
`;

const areArraysEqual = function(a, b) {
    if (a === b) return true;
    if (a.length !== b.length) return false;

    for (var i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) return false;
    }

    return true;
};

describe("Parsing", function() {
    it("can parse a table's headers", function() {
        var xmdt = new ExtendedMarkdownTable();

        var result = xmdt.parseHeaders(example1);
        var expected = [
            "name",
            "age",
            "job"
        ];
        chai.assert.ok(areArraysEqual(result, expected));

        result = xmdt.parseHeaders(example2);
        expected = [
            "city",
            "hood",
            "food",
            "commute",
            "rent"
        ];
        chai.assert.ok(areArraysEqual(result, expected));
    });

    it("can parse a Markdown table", function() {
        var xmdt = new ExtendedMarkdownTable();

        var result = xmdt.parseTable(example1, xmdt.parseHeaders(example1));
        var expected = [
            {
                name: "Mira",
                age: "27",
                job: "Superhero"
            }, {
                name: "Pietro",
                age: "24",
                job: "Clown"
            }, {
                name: "Sprocket",
                age: "17",
                job: "Robot"
            }
        ];
        chai.assert.ok(JSON.stringify(expected) === JSON.stringify(result));
    });

    it("can parse an extended table's functions", function() {
        var xmdt = new ExtendedMarkdownTable();

        // Example 1
        var resultFunctions = xmdt.parseFunctions(example1);
        var expected = {
            "title": (t, r, i) => `${r["name"]}, the ${r["job"]}`
        }
        chai.assert.ok(JSON.stringify(expected) === JSON.stringify(resultFunctions));

        var result = resultFunctions["title"]({}, {name: "Joe", age: "27", job: "Engineer"}, 0);
        expected = "Joe, the Engineer";
        chai.assert.ok(expected === result);

        // Example 2
        resultFunctions = xmdt.parseFunctions(example2);
        expected = {
            transportation: (t, r, i) => 23 * int(r["commute"]),
            total: (t, r, i) => parseInt(r["food"]) + parseInt(r["transportation"]) + parseInt(r["rent"])
        }
        chai.assert.ok(JSON.stringify(expected) === JSON.stringify(resultFunctions));

        result = resultFunctions["transportation"]({}, {commute: "10"}, 0);
        expected = 230;
        chai.assert.ok(expected === result);

        result = resultFunctions["total"]({}, {food: "100", transportation: "200", rent: "300"}, 0);
        expected = 600;
        chai.assert.ok(expected === result);
    });

    it("can extend a Markdown table", function() {
        var xmdt = new ExtendedMarkdownTable();

        var result = xmdt.extend(example1);
        var expected = [
            {
                name: "Mira",
                age: "27",
                job: "Superhero",
                title: "Mira, the Superhero"
            }, {
                name: "Pietro",
                age: "24",
                job: "Clown",
                title: "Pietro, the Clown"
            }, {
                name: "Sprocket",
                age: "17",
                job: "Robot",
                title: "Sprocket, the Robot"
            }
        ];
        chai.assert.ok(JSON.stringify(expected) === JSON.stringify(result));
    });
});

describe("Output", function() {
    it("should output Markdown strings", function() {
        var xmdt = new ExtendedMarkdownTable();
        var result = xmdt.toMarkdown(xmdt.extend(example1));
        var expected = `| name | age | job | title |
|----|----|----|----|
| Mira | 27 | Superhero | Mira, the Superhero |
| Pietro | 24 | Clown | Pietro, the Clown |
| Sprocket | 17 | Robot | Sprocket, the Robot |
`;
        chai.assert.ok(result === expected);
    });
});
